import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  ToastAndroid,
} from 'react-native';
import {
  Input,
  Text,
  Button,
  Select,
  SelectItem,
  IndexPath,
} from '@ui-kitten/components';
import firestore from '@react-native-firebase/firestore';

const StudentCreate = ({student, navigation}) => {
  const [id, setID] = useState('');
  const [name, setName] = useState('');
  const [sclass, setSClass] = useState('');
  const [sexSelectIndex, setSexSelectIndex] = useState(new IndexPath(0));
  const data = [
    {text: 'Male', value: true},
    {text: 'Female', value: false},
  ];
  const clearInputs = () => {
    setID('');
    setName('');
    setSClass('');
    setSexSelectIndex(new IndexPath(0));
  };
  const handleSubmit = () => {
    let sex = data[sexSelectIndex.row].value;
    const student = {id, name, sclass, sex};
    firestore()
      .collection('students')
      .add(student)
      .then(() => {
        ToastAndroid.show('Student was added', ToastAndroid.SHORT);
        clearInputs();
      });
  };
  return (
    <SafeAreaView>
      <View style={style.container}>
        <View style={style.formControlGap}>
          <View style={style.headerText}>
            <Text category="h4">New Student</Text>
          </View>
        </View>
      </View>
      <ScrollView style={style.container}>
        <View>
          <View style={style.formControlGap}>
            <Input
              label={(evaProps) => <Text {...evaProps}>ID</Text>}
              value={id}
              onChangeText={(text) => setID(text)}
            />
          </View>
          <View style={style.formControlGap}>
            <Input
              label={(evaProps) => <Text {...evaProps}>Name</Text>}
              value={name}
              onChangeText={(text) => setName(text)}
            />
          </View>
          <View style={style.formControlGap}>
            <Input
              label={(evaProps) => <Text {...evaProps}>Class</Text>}
              value={sclass}
              onChangeText={(text) => setSClass(text)}
            />
          </View>

          <View style={style.formControlGap}>
            <Select
              label={(evaProps) => <Text {...evaProps}>Sex</Text>}
              selectedIndex={sexSelectIndex}
              value={data[sexSelectIndex.row || 0].text}
              onSelect={(index) => setSexSelectIndex(index)}>
              <SelectItem title="Male"></SelectItem>
              <SelectItem title="Female"></SelectItem>
            </Select>
          </View>
        </View>
        <View>
          <View style={style.formControlGap}>
            <Button status="success" onPress={handleSubmit}>
              Save
            </Button>
          </View>
          <View style={style.formControlGap}>
            <Button
              status="danger"
              onPress={() => navigation.navigate('student_list')}>
              Cancel
            </Button>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  formControlGap: {
    marginVertical: 5,
  },
  headerText: {
    marginVertical: 10,
  },
});
export default StudentCreate;
