import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  ToastAndroid,
} from 'react-native';
import {
  Input,
  Text,
  Button,
  Select,
  SelectItem,
  IndexPath,
} from '@ui-kitten/components';
import firestore from '@react-native-firebase/firestore';

const StudentDetail = ({navigation, route}) => {
  const key = route.params.key;
  console.log(key);
  const [id, setID] = useState('');
  const [name, setName] = useState('');
  const [sclass, setSClass] = useState('');
  const [sexSelectIndex, setSexSelectIndex] = useState(
    // new IndexPath(route.params.student.sex ? 0 : 1),
    new IndexPath(0),
  );
  useEffect(() => {
    const subscriber = firestore()
      .collection('students')
      .doc(key)
      .onSnapshot((documentSnapshot) => {
        if (documentSnapshot._data) {
          let {id, name, sclass, sex} = documentSnapshot.data();
          setID(id);
          setName(name);
          setSClass(sclass);
          setSexSelectIndex(new IndexPath(sex ? 0 : 1));
        }
      });
    return () => subscriber();
  }, [key]);

  const handlesubmit = () => {
    let sex = data[sexSelectIndex.row].value;
    const student = {id, name, sclass, sex};
    firestore()
      .collection('students')
      .doc(key)
      .update(student)
      .then(() => {
        ToastAndroid.show('Student was updated', ToastAndroid.SHORT);
      });
  };

  const handleRemoval = () => {
    firestore()
      .collection('students')
      .doc(key)
      .delete()
      .then(() => {
        ToastAndroid.show('Student was deleted', ToastAndroid.SHORT);
        navigation.navigate('student_list');
      });
  };
  const data = [
    {text: 'Male', value: true},
    {text: 'Female', value: false},
  ];
  return (
    <SafeAreaView>
      <View style={style.container}>
        <View style={style.formControlGap}>
          <View style={style.headerText}>
            <Text category="h4">Student Detail</Text>
          </View>
        </View>
      </View>
      <ScrollView style={style.container}>
        <View>
          <View style={style.formControlGap}>
            <Input
              label={(evaProps) => <Text {...evaProps}>ID</Text>}
              value={id}
              onChangeText={(text) => setID(text)}
            />
          </View>
          <View style={style.formControlGap}>
            <Input
              label={(evaProps) => <Text {...evaProps}>Name</Text>}
              value={name}
              onChangeText={(text) => setName(text)}
            />
          </View>
          <View style={style.formControlGap}>
            <Input
              label={(evaProps) => <Text {...evaProps}>Class</Text>}
              value={sclass}
              onChangeText={(text) => setSClass(text)}
            />
          </View>

          <View style={style.formControlGap}>
            <Select
              label={(evaProps) => <Text {...evaProps}>Sex</Text>}
              selectedIndex={sexSelectIndex}
              value={data[sexSelectIndex.row || 0].text}
              onSelect={(index) => setSexSelectIndex(index)}>
              <SelectItem title="Male"></SelectItem>
              <SelectItem title="Female"></SelectItem>
            </Select>
          </View>
        </View>
        <View>
          <View style={style.formControlGap}>
            <Button status="success" onPress={handlesubmit}>
              Save
            </Button>
          </View>
          <View style={style.formControlGap}>
            <Button status="danger" onPress={handleRemoval}>
              Delete
            </Button>
          </View>
          <View style={style.formControlGap}>
            <Button
              status="info"
              onPress={() => navigation.navigate('student_add')}>
              Create new Student
            </Button>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  formControlGap: {
    marginVertical: 5,
  },
  headerText: {
    marginVertical: 10,
  },
});
export default StudentDetail;
