import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import {
  Input,
  Text,
  Select,
  SelectItem,
  IndexPath,
  Button,
} from '@ui-kitten/components';
import {DataTable} from 'react-native-paper';

import firestore from '@react-native-firebase/firestore';

const StudentList = ({navigation}) => {
  const [loading, setLoading] = useState(true);
  const [students, setStudents] = useState([]);
  useEffect(() => {
    const subscriber = firestore()
      .collection('students')
      .onSnapshot((querySnapshot) => {
        const students = [];

        querySnapshot.forEach((documentSnapshot) => {
          students.push({
            ...documentSnapshot.data(),
            key: documentSnapshot.id,
          });
        });

        setStudents(students);
        setLoading(false);
      });
    return () => subscriber();
  }, []);
  return (
    <SafeAreaView>
      <View style={style.container}>
        <View style={style.formControlGap}>
          <View style={style.headerWithControl}>
            <View style={style.headerText}>
              <Text category="h4">Student List</Text>
            </View>
            <View style={style.control}>
              <Button size='small' onPress={() => navigation.navigate('student_add')}>New</Button>
            </View>
          </View>
        </View>
      </View>
      {loading ? (
        <ActivityIndicator />
      ) : (
        <ScrollView>
          <DataTable>
            <DataTable.Header>
              <DataTable.Title>ID</DataTable.Title>
              <DataTable.Title>Name</DataTable.Title>
              <DataTable.Title numeric>Class</DataTable.Title>
              <DataTable.Title numeric>Sex</DataTable.Title>
            </DataTable.Header>
            {students.map((item, index) => (
              <DataTable.Row
                key={index}
                onPress={() =>
                  navigation.navigate('student_detail', {key: item.key})
                }>
                <DataTable.Cell>{item.id}</DataTable.Cell>
                <DataTable.Cell>{item.name}</DataTable.Cell>
                <DataTable.Cell numeric>{item.sclass}</DataTable.Cell>
                <DataTable.Cell numeric>
                  {item.sex ? 'Male' : 'Female'}
                </DataTable.Cell>
              </DataTable.Row>
            ))}
            {/* 
            <DataTable.Pagination
              page={1}
              numberOfPages={3}
              onPageChange={(page) => {
                console.log(page);
              }}
              label="1-2 of 6"
            /> */}
          </DataTable>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  formControlGap: {
    marginVertical: 5,
  },
  headerText: {
    marginVertical: 10,
  },
  control: {
    width: 80,
  },
  headerWithControl: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    alignItems: 'center'
  }
});
export default StudentList;
